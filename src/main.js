import Vue from 'vue'

import router from '@/router'
import App from '@/App.vue'
import '@/assets/css/main.scss'

import VueFire from 'vuefire'
import firebase from '@/plugins/firebase'

Vue.config.productionTip = true
Vue.use(VueFire)
Vue.use(firebase)

new Vue({
  render: h => h(App),
  router
}).$mount('#app')