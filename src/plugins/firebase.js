import Vue from 'vue'

import VueFire from 'vuefire'
import firebase from 'firebase'
import 'firebase/firestore'

const config = {
  apiKey: "AIzaSyB68_JgJju8MqCsODfOiE18-2b-HrDY1nY",
  authDomain: "gnome-game-hub.firebaseapp.com",
  databaseURL: "https://gnome-game-hub.firebaseio.com",
  projectId: "gnome-game-hub",
  storageBucket: "gnome-game-hub.appspot.com",
  messagingSenderId: "970375210739"
}

Vue.use(VueFire)

export default {
  install(Vue) {
    firebase.initializeApp(config)
    let firestore = firebase.firestore()
    let settings = {timestampsInSnapshots: true}
    firestore.settings(settings)
    Vue.prototype.$firebase = firebase
    Vue.prototype.$db = firestore
  }
}