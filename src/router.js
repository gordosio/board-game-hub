import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home.vue'
import About from '@/views/About.vue'

import Spyfall from '@/views/spyfall/Spyfall.vue'
import Werewolf from '@/views/werewolf/Werewolf.vue'
import Avalon from '@/views/avalon/Avalon.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/about',
      component: About
    },
    {
      path: '/spyfall',
      component: Spyfall
    },
    {
      path: '/spyfall/:id',
      component: Spyfall
    },
    {
      path: '/werewolf',
      component: Werewolf
    },
    {
      path: '/avalon',
      component: Avalon
    },
  ]
})
